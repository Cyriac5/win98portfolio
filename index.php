<?php
    // Initialisation des variables pour récupérer les valeurs entrées dans le formulaire. 
		// Deux cas possibles, soit c'est ma première visite, je n'ai pas encore entré de données, auquel cas ces variables seront vides, soit je viens d'envoyer le formulaire, et donc je suis rediriger à nouveau sur la page et ces variables contiennent quelque chose. (Ce qui est géré en PHP dans la partie HTML, à l'interieur des "value" de chaque input.)

    $firstname = $email = $phone = $message = "";

    // Déclaration des variables Error pour ces mêmes inputs.
    $firstnameError = $emailError = $phoneError = $messageError = "";
    $isSuccess = false;

    // Envoi de l'email :

    $emailTo = "a.cyriac@hotmail.fr";

  ##############     RECUPERATION DES DONNÉES     ###############
    if($_SERVER["REQUEST_METHOD"] == "POST"){
        $firstname = verifyInput($_POST['firstname']);
        $email = verifyInput($_POST['email']);
        $phone = verifyInput($_POST['phone']);
        $message = verifyInput($_POST['message']);
        // Une fois le formulaire envoyé, le isSuccess est true.
        $isSuccess = true;
        $emailText = "";

	#################### LES FONCTIONS : #####################

    // Particularité pour l'email et le téléphone :
    function isEmail($adresseEmail){
        return filter_var($adresseEmail, FILTER_VALIDATE_EMAIL);
    }

    // Vérification du téléphone avec une expression régulière (REGEXP) :
    function isPhone($numero){
        // On comprend ici les chiffres compris entre 0 et 9 ainsi que les espaces (entre le 9 et le crochet).
        return preg_match("/^[0-9 ]*$/", $numero);
    }
    
    // Pensons à la sécurité : 
    function verifyInput($varToVerify){
        // trim() enlève tous les espaces, les tabulations, les retours à la ligne des champs du formulaire.
        $varToVerify = trim($varToVerify);
        // stripcslashes va retirer tous les antislash
        $varToVerify = stripcslashes($varToVerify);
        // Contre les Cross-Site-Scripting (XSS)
        $varToVerify = htmlspecialchars($varToVerify);

        return $varToVerify;
    }

        ############    VALIDATION / VERIFICATION :    ################
        // On a toujours besoin d'une validation qu'elle soit côté client (required dans le formulaire) ou côté serveur.
        // La validation côté client peut être contournée via l'inspecteur. La validation côté serveur est plus sécure. 

        if(empty($_POST['firstname'])){
            $firstnameError = "I need to know your firstname";
            $isSuccess = false;
        } else {
            // Si je n'ai pas d'erreur, je récupère ma variable text email et je la concatène avec les données récupérées.
            $emailText .= "Firstname : $firstname\n";
        }
        if(!isEmail($email)){
            $emailError = "Are you kidding me ?";
            $isSuccess = false;
        } else {
            $emailText .= "Email : $email\n";
        }

        if(!isPhone($phone)){
            $phoneError = "Incorrect phone number.";
            $isSuccess = false;
        } else {
            $emailText .= "Phone : $phone\n";
        }

        if(empty($_POST['message'])){
            $messageError = "Whaaaaat ?";
            $isSuccess = false;
        } else {
            $emailText .= "Message : $message\n";
        }

        if($isSuccess){
            // ENVOI DE L'EMAIL Si je n'ai eu aucun message d'erreur :
            $headers = "From: $firstname <$email>\r\nReply-To: $email";
            mail($emailTo,"Formulaire de contact en PHP", $emailText, $headers);

            // Lorsque l'email a été envoyé, je remets tous mes champs à 0. 
            $firstname = $email = $phone = $message = "";
        }
    }

 
    

?>

<!DOCTYPE html>
<html>
    <head>
        <title>Contactez-Moi !</title>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
        <link href="http://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
    </head>
    
    
    <body>
        
       <div class="container">
            <div class="heading">
                <h2>Contactez-moi</h2>
            </div>
                
           <div class="row">
               <div class="col-lg-10 col-lg-offset-1">
               <!-- $_SERVER['PHP_SELF']; renvoi vers la page courante, donc l'index.php, vous pouvez le vérifier en inspectant votre code. -->
                    <form id="contact-form" method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="firstname">Prénom</label>
                                <input id="firstname" type="text" name="firstname" class="form-control" value="<?php echo $firstname ; ?>">
                                <p class="comments"><?php echo $firstnameError;?></p>
                            </div>
                            <div class="col-md-6">
                                <label for="email">Email</label>
                                <input id="email" type="email" name="email"  class="form-control" 
                                value="<?php echo $email ; ?>">
                                <p class="comments"><?php echo $emailError;?></p>
                            </div>
                            <div class="col-md-6">
                                <label for="phone">Téléphone</label>
                                <input id="phone" type="tel" name="phone" class="form-control" value="<?php echo $phone ; ?>">
                                <p class="comments"><?php echo $phoneError;?></p>
                            </div>
                            <div class="col-md-12">
                                <label for="message">Message</label>
                                <textarea id="message" name="message" class="form-control" rows="4" value=""></textarea>
                                <p class="comments"><?php echo $messageError;?></p>
                            </div>
                            <div class="col-md-12">
                                <input type="submit" class="button1" value="Envoyer">
                            </div>    
                        </div>
                        <!-- On gère ici l'affichage du message d'envoie via un ternaire en PHP : -->
                        <p style="display: <?php echo ($isSuccess)?  'block' : 'none'; ?> ">Message envoyé !</p>
                    </form>
                </div>
           </div>
        </div>
        
    </body>

</html>
